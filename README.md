# Windows Terminal

My configuration and customization of [Windows Terminal](https://github.com/microsoft/terminal).

### Terminal Hand Drawn Symbol icon license

This project uses a modified version of the [Terminal Hand Drawn Symbol Icon](https://www.pngrepo.com/svg/24334/terminal-hand-drawn-symbol) 
under a [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
license.
